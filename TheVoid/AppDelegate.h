//
//  AppDelegate.h
//  TheVoid
//
//  Created by Brian Gerfort on 05/05/2017.
//  Copyright © 2017 2ndIttyBitty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

