//
//  main.m
//  TheVoid
//
//  Created by Brian Gerfort on 05/05/2017.
//  Copyright © 2017 2ndIttyBitty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
