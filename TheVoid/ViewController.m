//
//  ViewController.m
//  TheVoid
//
//  Created by Brian Gerfort on 05/05/2017.
//  Copyright © 2017 2ndIttyBitty. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor cyanColor]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
